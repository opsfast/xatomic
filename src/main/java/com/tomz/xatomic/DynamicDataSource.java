package com.tomz.xatomic;

import java.lang.annotation.*;

/**
 * 数据源注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DynamicDataSource {
    /**
     * 数据源名称
     * @return
     */
    String value() default "default";
}
