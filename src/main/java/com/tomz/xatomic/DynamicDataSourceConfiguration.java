package com.tomz.xatomic;

import com.tomz.xatomic.xa.XaDynamicDataSourceConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * @author ZHUFEIFEI
 */
@Configuration
@ConditionalOnMissingBean({XaDynamicDataSourceConfiguration.class})
@ConditionalOnProperty(prefix = "spring.dynamic.datasource", name = "enabled", matchIfMissing = true)
@Import({DynamicDataSourceRegistrar.class})
public class DynamicDataSourceConfiguration implements DynamicDataSourceInitializer{
}